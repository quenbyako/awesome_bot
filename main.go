package main

// это всякие библиотеки нужные нам для работы программы
import (
	"fmt"
	"os"
	"regexp"
	"strings"
	"time"

	"github.com/pkg/errors"
	"github.com/ungerik/go-dry"
	"github.com/valyala/fasttemplate"

	"github.com/labstack/echo"
	"github.com/ololosha228/ifttt"
	"github.com/ololosha228/keystorage"
)

const (
	KeyStoragewPath = "./keys.json"
)

var (
	ks          *keystorage.PrimitiveStorage
	iftttClient ifttt.Client
)

func init() {
	var err error
	if dry.FileExists(KeyStoragewPath) {
		ks, err = keystorage.OpenPrimitive("ifttt", KeyStoragewPath)
		if err != nil {
			panic(err)
		}
	} else {
		ks = keystorage.NewPrimitive("ifttt")
	}

	periodicallyDumpStorage(ks)

	iftttClient, err = ifttt.NewClient(ifttt.ClientConfig{
		KeyStorage: ks,
	})
	dry.PanicIfErr(err)
}

func main() {
	// долго объяснять что это снизу, короче это костыль, который решает проблему с адресами хероку
	port := os.Getenv("PORT")
	if port == "" {
		if runningOnHeroku() {
			panic("переменная $PORT не установлена. хотя вообще-то должна")
		}

		port = "8989"
	}

	b := NewBotHandler() // вот это мы создаем обработчика для чатбота.
	// BotHandler будет нам помогать запускать определенные действия если сработал нужный нам интент

	//////////////////////////////////////////////////////////////
	// вот сюда прописываем на какой интент нам реагировать и какой функцией

	// вот так ты вставляешь
	// b.On("movie_ask:time", ThatIsMyResponse)
	// все что отмечено слешами -> "//" <- это комментарии, программа не будет считать текст за комментарием как команду
	b.OnFallback(FallbackHandler)

	//////////////////////////////////////////////////////////////

	// вот тут мы запускаем сервер, который будет слушать все запросы от dialogflow
	api := echo.New()
	api.Any("/", b.CatchRequest) // а это мы говорим, что запросы от dialogflow должен обрабатывать наш бот

	dry.PanicIfErr(api.Start(":" + port)) // запускаем наш сервер, и ждем пока чатбот отправит на него запросы
}

func runningOnHeroku() bool {
	if len(os.Args) < 2 {
		return false
	}
	return os.Args[1] == "--heroku"
}

func IftttHandler(req *DialogflowRequest) string {
	if req.OriginalDetectIntentRequest.Source != "telegram" {
		return "ifttt будет работать только с клиентом в телеграме. без вариантов."
	}

	splitted := strings.SplitN(req.QueryResult.Action, ":", 2)
	if len(splitted) != 2 {
		return "ERROR: некорректно передано название эвента для ifttt"
	}

	triggername := splitted[1]

	data := make([]string, 3)
	data[0] = fmt.Sprint(req.QueryResult.Params.Get("value1"))
	data[1] = fmt.Sprint(req.QueryResult.Params.Get("value2"))
	data[2] = fmt.Sprint(req.QueryResult.Params.Get("value3"))

	userid := req.OriginalDetectIntentRequest.Payload.From.ID
	if _, err := ks.UserKey(userid, "ifttt"); err != nil {
		return "сначала отправь мне токен командой /ifttt"
	}

	err := iftttClient.By(userid).Trigger(triggername, data...)
	if err != nil {
		return errors.Wrap(err, "can't run ifttt event").Error()
	}

	return req.QueryResult.FullfilmentText
}

func InternalActionsHandler(req *DialogflowRequest) string {
	switch strings.TrimPrefix(req.QueryResult.Action, "internal:") {
	case "time.get":
		now := time.Now()

		t := fasttemplate.New(req.QueryResult.FullfilmentText, "#{", "}")
		s := t.ExecuteString(map[string]interface{}{
			"hour":   now.Hour(),
			"minute": now.Minute(),
		})
		return s
	}

	return req.QueryResult.FullfilmentText
}

func FallbackHandler(req *DialogflowRequest) string {
	if strings.HasPrefix(req.QueryResult.QueryText, "/ifttt") {
		token := strings.TrimPrefix(req.QueryResult.QueryText, "/ifttt")
		token = strings.TrimSpace(token)
		if ok, _ := regexp.MatchString("^[a-zA-Z0-9-]{22}$", token); !ok {
			return "кажется, токен невалидный."
		}

		ks.Set(req.OriginalDetectIntentRequest.Payload.From.ID, token)

		return "ключ от ifttt сохранен."
	}

	return "Кажется, я тебя не совсем понимаю."
}

func periodicallyDumpStorage(ks *keystorage.PrimitiveStorage) {
	go func() {
		for {
			time.Sleep(2 * time.Minute)
			dry.PanicIfErr(ks.Dump(KeyStoragewPath))
		}
	}()
}
