FROM ubuntu:18.04
RUN apt update && apt install -y ca-certificates
COPY awesome_bot /bin/awesome_bot
CMD [ "/bin/awesome_bot" ]