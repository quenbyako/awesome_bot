.PHONY: push

push:
	go mod tidy
	go mod vendor
	git add -A .
	git commit -m .
	git push heroku master
