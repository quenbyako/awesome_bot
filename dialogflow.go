// это файлик который описывает алгоритм обработки запросов из dialogflow на низком уровне
// можешь поизучать его устройство, но главное не трогай тут ничего и не меняй
//
// этот файл позволяет не ебаться тебе с обработкой низкоуровневых запросов и их декобированием
//
//
//
//
// НЕ РЕДАКТИРУЙ ЭТОТ ФАЙЛ!!!!
// НЕ РЕДАКТИРУЙ ЭТОТ ФАЙЛ!!!!
// НЕ РЕДАКТИРУЙ ЭТОТ ФАЙЛ!!!!
// НЕ РЕДАКТИРУЙ ЭТОТ ФАЙЛ!!!!
// НЕ РЕДАКТИРУЙ ЭТОТ ФАЙЛ!!!!
package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"strconv"
	"strings"

	"github.com/labstack/echo"
)

// этот кусочек описывает, какие именно данные приходят от dialogflow
// вообще есть документация у гугла, но она корявая, и данные по факту приходят другие
//
// в функциях ты работаешь именно с этими данными.
type DialogflowRequest struct {
	Session                     string          `json:"session"`
	ResponseID                  string          `json:"responseId"`
	QueryResult                 QueryResult     `json:"queryResult"` // вон там ниже посмотри, что тут хранится
	OriginalDetectIntentRequest OriginalRequest `json:"originalDetectIntentRequest"`
}

type QueryResult struct {
	QueryText                 string    `json:"queryText"`                 // текст который написал пользователь боту
	Action                    string    `json:"action"`                    // экшен который мы в интенте  прописали
	Params                    Params    `json:"parameters"`                // сущности которые мы вытянули из текста
	OutputContexts            []Context `json:"outputContexts"`            // контекст диалога, который у нас есть сейчас
	Intent                    Intent    `json:"intent"`                    // название интента, который стриггернулся
	IntentDetectionConfidence float64   `json:"intentDetectionConfidence"` // какова точность, что чатбот правильно определил интент (от 0 до 1)
	FullfilmentText           string    `json:"fulfillmentText"`           // что нужно ответить если запрос успешен
}

type Params map[string]Param

func (p Params) Get(item string) string {
	v, ok := p[item]
	if ok {
		return fmt.Sprint(v)
	}
	return ""
}

type OriginalRequest struct {
	Source  string                  `json:"source"`
	Payload *OriginalRequestPayload `json:"payload"`
}

type OriginalRequestPayload struct {
	Chat struct {
		Type string `json:"type"`
		ID   string `json:"id"`
	} `json:"chat"`
	From struct {
		ID        string `json:"id"`
		Username  string `json:"username"`
		FirstName string `json:"first_name"`
		LastName  string `json:"last_name"`
	} `json:"from"`
}

func (o *OriginalRequestPayload) UnmarshalJSON(data []byte) error {
	var res map[string]interface{}
	err := json.Unmarshal(data, &res)
	if err != nil {
		return err
	}

	obj, ok := res["data"]
	if !ok {
		return errors.New("invalid object")
	}

	for k, v := range obj.(map[string]interface{}) {
		if k == "from" {
			val := v.(map[string]interface{})
			o.From.ID = strconv.Itoa(int(val["id"].(float64)))
			o.From.Username = val["username"].(string)
			o.From.FirstName = val["first_name"].(string)
			o.From.LastName = val["last_name"].(string)
		}
		if k == "chat" {
			val := v.(map[string]interface{})
			o.Chat.ID = val["id"].(string)
			o.Chat.Type = val["type"].(string)
		}
	}

	return nil
}

type Intent struct {
	Name        string `json:"name"`        // это короче айдишник, но его назвали именем
	DisplayName string `json:"displayName"` // а вот это реальное название интента
	IsFallback  bool   `json:"isFallback"`
}

type Param interface{} // потому что блять он выдает разные типы, потом переделаю
//type Param struct {
//	Amount interface{} `json:"amount"`
//	Unit   string      `json:"unit"`
//}

type Context struct {
	Name   string
	Params map[string]Param
}

// все что ниже -- не трогай, это кусочек обработчика запросов от dialogflow

// НИЖЕ ЗАПРЕТНАЯ ЗОНА! съеби отсюда нахой
type DialogflowResponse struct {
	FulfillmentText string `json:"fulfillment_text"`
}

type IntentHandler func(req *DialogflowRequest) string

type BotHandler struct {
	intents  map[string]IntentHandler
	actions  map[string]IntentHandler
	fallback IntentHandler
}

func NewBotHandler() *BotHandler {
	b := new(BotHandler)
	b.intents = make(map[string]IntentHandler)
	b.actions = make(map[string]IntentHandler)
	return b
}

func (b *BotHandler) OnFallback(i IntentHandler) {
	if b.fallback != nil {
		panic("фолбэк описан дважды")
	}
	b.fallback = i
}

func (b *BotHandler) CatchRequest(c echo.Context) error {
	if c.Request().UserAgent() != "Google-Dialogflow" {
		return c.String(200, "это фулфиллмент сервер для dialogflow. можешь подключить этот сайт в fulfillment у своего чатбота!")
	}

	data, err := ioutil.ReadAll(c.Request().Body)
	c.Request().Body.Close()
	if err != nil {
		fmt.Println(err)
		return c.String(500, err.Error())
	}

	if len(data) == 0 {
		println("JSON no content")
		return c.NoContent(200)
	}

	request := new(DialogflowRequest)
	err = json.Unmarshal(data, request)
	if err != nil {
		println("ПРОБЛЕМЕСЫ: " + err.Error())
	}

	for_show, _ := json.MarshalIndent(request, "", "    ")
	fmt.Println(string(for_show))

	if request.QueryResult.Intent.IsFallback {
		if b.fallback == nil {
			c.String(500, "фолбэк не назначен")
		}

		response := b.fallback(request)
		return c.JSON(200, &DialogflowResponse{response})
	}

	if request.QueryResult.Action != "" {
		if strings.HasPrefix(request.QueryResult.Action, "ifttt:") {
			return c.JSON(200, &DialogflowResponse{IftttHandler(request)})
		}

		if strings.HasPrefix(request.QueryResult.Action, "internal:") {
			return c.JSON(200, &DialogflowResponse{InternalActionsHandler(request)})
		}
	}

	f, ok := b.intents[request.QueryResult.Intent.DisplayName]
	if !ok {
		return c.String(400, "упс, этот интент не требует вебхука. можешь отключить")
	}

	return c.JSON(200, &DialogflowResponse{f(request)})
}
